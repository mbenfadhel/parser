package com.ritmx.parser.exception;

public class SearchedHourException extends Exception{

	private static final long serialVersionUID = 8533136570547024858L;

	/**
	 * 
	 */
	public SearchedHourException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public SearchedHourException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public SearchedHourException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public SearchedHourException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public SearchedHourException(Throwable cause) {
		super(cause);
	}
	
	

}
