package com.ritmx.parser.service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ritmx.parser.exception.SearchedHourException;
import com.ritmx.util.DateUtil;

public class ParserServiceImpl implements ParserService{

	private static final Logger LOG = LogManager.getLogger(ParserServiceImpl.class);
	private String maxConnectedServer = "";
	private int numberOfMaxConnections = -1;
	private int numberOfConnections = 0;
	private String fileName;
	Properties prop = new Properties();
	InputStream input = null;

	public ParserServiceImpl() {
		input = getClass().getClassLoader().getResourceAsStream("config.properties");
		// load a properties file
		try {
			prop.load(input);
			fileName = prop.getProperty("filename");
		} catch (IOException e) {
			LOG.error(e.getMessage());
		}
	}

	public Set<String> getServersConnectedTo(String serverToFetch, int hour) throws SearchedHourException {
		if (hour > 24 || hour < 0) {
			throw new SearchedHourException("L'heure à laquelle vous souhaitez rechercher est invalide");
		}
		try {
			Stream<String> stream = Files.lines(Paths.get(fileName));
			Set<String> parserList = stream.filter(filteredFile -> {
				String[] line = filteredFile.split(" ");
				LocalDateTime timeStamp = DateUtil.fromLongToLocalDateTime(Long.valueOf(line[0]));
				boolean equalsToHour = timeStamp.getHour() == hour ? true : false;
				boolean isServerToFetch = serverToFetch.equals(line[2]) ? true : false;
				return equalsToHour && isServerToFetch;
			}).map(file -> {
				String[] line = file.split(" ");
				return line[1];
			}).collect(Collectors.toSet());
			stream.close();
			return parserList;
		} catch (IOException e) {
			LOG.error(e.getMessage());
		}
		return null;
	}

	public Set<String> getServersWhoServerConnectsTo(String serverHaveConnect, int hour) throws SearchedHourException {
		if (hour > 24 || hour < 0) {
			throw new SearchedHourException("L'heure à laquelle vous souhaitez rechercher est invalide");
		}
		try {
			Stream<String> stream = Files.lines(Paths.get(fileName));
			Set<String> parserList = stream.filter(filteredFile -> {
				String[] line = filteredFile.split(" ");
				LocalDateTime timeStamp = DateUtil.fromLongToLocalDateTime(Long.valueOf(line[0]));
				boolean equalsToHour = timeStamp.getHour() == hour ? true : false;
				boolean isServerHaveConnect = serverHaveConnect.equals(line[1]) ? true : false;
				return equalsToHour && isServerHaveConnect;
			}).map(file -> {
				String[] line = file.split(" ");
				return line[2];
			}).collect(Collectors.toSet());
			stream.close();
			return parserList;
		} catch (IOException e) {
			LOG.error(e.getMessage());
		}
		return null;
	}

	
	public String serverHaveMaxConnection(int hour) throws SearchedHourException {
		if (hour > 24 || hour < 0) {
			throw new SearchedHourException("L'heure à laquelle vous souhaitez rechercher est invalide");
		}
		try {
			Stream<String> stream = Files.lines(Paths.get(fileName));
			stream.filter(filteredFile -> {
				String[] line = filteredFile.split(" ");
				LocalDateTime timeStamp = DateUtil.fromLongToLocalDateTime(Long.valueOf(line[0]));
				boolean equalsToHour = timeStamp.getHour() == hour ? true : false;
				return equalsToHour;
			}).map(file -> {
				String[] line = file.split(" ");
				return line[1];
			}).sorted().forEach(server -> {
				if (maxConnectedServer.equals(server)) {
					numberOfConnections = numberOfConnections + 1;
				} else {
					if (!maxConnectedServer.equals(server) && numberOfConnections > numberOfMaxConnections) {
						maxConnectedServer = server;
						numberOfMaxConnections = numberOfConnections;
						numberOfConnections = 0;
					}
				}

			});
			String message = "Le serveur : " + maxConnectedServer + " a sollicité une connection "
					+ numberOfMaxConnections + " fois";
			stream.close();
			return message;
		} catch (IOException e) {
			LOG.error(e.getMessage());
		}
		return null;
	}
}
