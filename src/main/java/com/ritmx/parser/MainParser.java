package com.ritmx.parser;

import java.util.Scanner;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ritmx.parser.exception.SearchedHourException;
import com.ritmx.parser.service.ParserService;
import com.ritmx.parser.service.ParserServiceImpl;

public class MainParser {
	private static final Logger LOG = LogManager.getLogger(ParserServiceImpl.class);

	public void display_menu() {
		System.out.println("******************************************************");
		System.out.println("*************			MENU			***********");
		System.out.println("******************************************************");
		System.out.println("0:Afficher le menu");
		System.out.println("1:la liste des serveurs qui ont été connecté à un serveur donné durant cette heure");
		System.out.println("2:la liste des serveurs auxquels un serveur donné s'est connecté durant cette heure");
		System.out.println("3:le serveur qui a généré le plus de connections durant cette heure");
		System.out.println("4:arreter l'execution");
		System.out.println("******************************************************");
	}

	public MainParser() {
		Scanner in = new Scanner(System.in);
		display_menu();
		ParserService parserService = new ParserServiceImpl();
		boolean execute = true;
		do {
			switch (in.nextInt()) {
			case 0:
				display_menu();
				break;
			case 1:
				System.out.print("entrez serveur recherché : ");
				System.out.println("");
				Scanner serverToFetchIn = new Scanner(System.in);
				Scanner hourIn = null;
				if (serverToFetchIn.hasNext()) {
					System.out.print("entrez l'heure d'execution : ");
					hourIn = new Scanner(System.in);
					System.out.println("");
				}
				try {
					Set<String> parserList = parserService.getServersConnectedTo(serverToFetchIn.next(), hourIn.nextInt());
					parserList.forEach(System.out::println);
				} catch (SearchedHourException e) {
					LOG.error(e.getMessage());
				}
				serverToFetchIn.reset();
				hourIn.reset();
				break;
			case 2:
				System.out.print("entrez le serveur recherché : ");
				System.out.println("");
				serverToFetchIn = new Scanner(System.in);
				hourIn = null;
				if (serverToFetchIn.hasNext()) {
					System.out.print("entrez l'heure d'execution : ");
					hourIn = new Scanner(System.in);
					System.out.println("");
				}
				try {
					Set<String> parserList = parserService.getServersWhoServerConnectsTo(serverToFetchIn.next(), hourIn.nextInt());
					parserList.forEach(System.out::println);
				} catch (SearchedHourException e) {
					LOG.error(e.getMessage());
				}
				serverToFetchIn.reset();
				hourIn.reset();
				break;
			case 3:
				System.out.print("entrez l'heure d'execution : ");
				hourIn = new Scanner(System.in);
				System.out.println("");
				try {
					String message = parserService.serverHaveMaxConnection(hourIn.nextInt());
					System.out.println(message);
				} catch (SearchedHourException e) {
					LOG.error(e.getMessage());
				}
				hourIn.reset();
				break;
			case 4:
				execute = false;
				break;
			default:
				System.err.println("Option non reconnue");
				break;
			}
			in = in.reset();
		} while (execute);
	}

	public static void main(String[] args) {
		new MainParser();
	}

}
