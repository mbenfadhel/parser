package com.ritmx.util;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DateUtil {
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

	public static LocalDateTime fromStringToLocalDateTime(String dateStr) {
		LocalDateTime dateTime = LocalDateTime.parse(dateStr, formatter);
		return dateTime;
	}
	
	public static LocalDateTime fromLongToLocalDateTime(long dateAsLong) {
		LocalDateTime dateTime =
			    LocalDateTime.ofInstant(Instant.ofEpochSecond(dateAsLong), ZoneId.systemDefault());
		return dateTime;
	}

	public static String fromLocalDateTimeToString(int year, Month month, int dayOfMonth, int hour, int minute) {
		LocalDateTime dateTime = LocalDateTime.of(year, month, dayOfMonth, hour, minute);
		String formattedDateTime = dateTime.format(formatter);
		return formattedDateTime;
	}
	
	public static long fromLocalDateTimeToLong(int year, Month month, int dayOfMonth, int hour, int minute) {
		LocalDateTime dateTime = LocalDateTime.of(year, month, dayOfMonth, hour, minute);
		ZoneId zoneId = ZoneId.systemDefault(); // or: ZoneId.of("Europe/Oslo");
		long epoch = dateTime.atZone(zoneId).toEpochSecond();
		return epoch;
	}

}
