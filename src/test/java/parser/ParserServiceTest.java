package parser;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.ritmx.parser.exception.SearchedHourException;
import com.ritmx.parser.service.ParserService;
import com.ritmx.parser.service.ParserServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ParserServiceTest {

	ParserService parserService;

	@Before
	public void init() {
		parserService = new ParserServiceImpl();
	}

	@Test
	public void getServersConnectedToTest() {
		try {
			Set<String> parserList = parserService.getServersConnectedTo("garak", 15);
			assertEquals(parserList.size(), 2);
		} catch (SearchedHourException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void getServersWhoServerConnectsToTest() {
		try {
			Set<String> parserList = parserService.getServersWhoServerConnectsTo("quark", 15);
			assertEquals(parserList.size(), 1);
		} catch (SearchedHourException e) {
			e.printStackTrace();
		}
	}

	@Test()
	public void serverHaveMaxConnectionTest() {
		try {
			String excpectedMessage = "Le serveur : lilac a sollicité une connection 11 fois";
			String message = parserService.serverHaveMaxConnection(15);
			assertEquals(excpectedMessage, message);
		} catch (SearchedHourException e) {
			e.printStackTrace();
		}
	}

	@Test(expected = SearchedHourException.class)
	public void getServersConnectedToExceptionTest() throws SearchedHourException {
		parserService.getServersConnectedTo("garak", 35);
	}

	@Test(expected = SearchedHourException.class)
	public void getServersWhoServerConnectsToExceptionTest() throws SearchedHourException {
		parserService.getServersWhoServerConnectsTo("quark", 35);
	}

	@Test(expected = SearchedHourException.class)
	public void serverHaveMaxConnectionExceptionTest() throws SearchedHourException {
		parserService.serverHaveMaxConnection(35);
	}

}
