package com.ritmx.parser.service;

import java.util.Set;

import com.ritmx.parser.exception.SearchedHourException;

public interface ParserService {

	/**
	 * la liste des serveurs qui ont été connecté à un serveur donné durant cette
	 * heure
	 * 
	 * @param serverToFetch
	 * @param hour
	 * @throws SearchedHourException
	 */
	public Set<String> getServersConnectedTo(String serverToFetch, int hour) throws SearchedHourException;

	/**
	 * la liste des serveurs auxquels un serveur donné s'est connecté durant cette
	 * heure
	 * 
	 * @param serverHaveConnect
	 * @param hour
	 * @throws SearchedHourException
	 */
	public Set<String> getServersWhoServerConnectsTo(String serverHaveConnect, int hour) throws SearchedHourException;

	/**
	 * le serveur qui a généré le plus de connections durant cette heure
	 * 
	 * @param hour
	 * @throws SearchedHourException
	 */
	public String serverHaveMaxConnection(int hour) throws SearchedHourException;

}
